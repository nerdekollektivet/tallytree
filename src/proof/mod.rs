/// Data structures for merkle tally tree proofs.
pub mod define;

/// Create and verify proofs of vote count in a merkle tally tree. This can
/// be used to check that there is no mismatch between tallied votes and votes
/// in the merkle tally tree.
pub mod votecount;

/// Create and verify proof of a vote being included OR excluded in/from a
/// merkle tally tree.
pub mod inclusion;

/// Helper functions for creating merkle tree proofs.
pub mod util;

pub use crate::proof::define::*;
pub use crate::proof::inclusion::*;
pub use crate::proof::votecount::*;
