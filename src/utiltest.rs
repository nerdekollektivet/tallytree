use crate::tally::TallyList;
use crate::VoteReference;

/**
 * Use number as prefix of dummy hash.
 */
pub fn dummy_hash_from_number(n: u128) -> [u8; 32] {
    let mut hash = [0x00; 32];
    for (i, b) in n.to_be_bytes().into_iter().enumerate() {
        hash[i] = b;
    }
    hash
}

/// Create a list of dummy votes with given size.
pub fn create_votes(votes: usize) -> Vec<(VoteReference, TallyList)> {
    let vote_a = [1, 0];
    let vote_b = [0, 1];

    // We step over odd numbers to have gaps. This allows testing excluded votes.
    (0..(votes * 2) as u128)
        .step_by(2)
        .map(|v: u128| {
            let reference = dummy_hash_from_number(v);
            let vote = if v % 2 == 0 { vote_a } else { vote_b };
            (reference, vote.to_vec())
        })
        .collect()
}
#[cfg(test)]
mod tests {
    use super::create_votes;
    #[cfg(feature = "with-benchmarks")]
    use test::Bencher;

    #[test]
    fn test_create_votes() {
        let votes = create_votes(10_000);
        assert_eq!(10_000, votes.len());
    }

    #[test]
    fn test_create_vote_no_odds() {
        let votes = create_votes(10);
        for (v, _) in votes {
            let v = u128::from_be_bytes(v[0..16].try_into().unwrap());
            assert!(v % 2 == 0);
        }
    }

    #[cfg(feature = "with-benchmarks")]
    #[bench]
    fn bench_create_votes_1k(b: &mut Bencher) {
        b.iter(|| create_votes(1_000))
    }

    #[cfg(feature = "with-benchmarks")]
    #[bench]
    fn bench_create_votes_10k(b: &mut Bencher) {
        b.iter(|| create_votes(10_000))
    }

    #[cfg(feature = "with-benchmarks")]
    #[bench]
    fn bench_create_votes_100k(b: &mut Bencher) {
        b.iter(|| create_votes(100_000))
    }
}
