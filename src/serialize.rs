#[cfg(feature = "with-serde")]
use serde::{Deserialize, Serialize};

/// Serialize a node (including all child nodes) into JSON.
///
/// Example with merkle tree:
/// ```
/// use tallytree::serialize::to_json;
/// use tallytree::generate::generate_tree;
/// let tree = generate_tree(vec![
///     ([0xaa; 32], vec![1, 0]),
///     ([0xbb; 32], vec![0, 1]),
/// ], false).unwrap();
/// let tree_json: String = to_json(&tree).unwrap();
/// ```
///
/// Example with proofs:
/// ```
/// use tallytree::serialize::{to_json, from_json};
/// use tallytree::proof::*;
/// use tallytree::generate::generate_tree;
/// use tallytree::Validation;
/// let tree = generate_tree(vec![
///     ([0xaa; 32], vec![1, 0]),
///     ([0xbb; 32], vec![1, 0]),
///     ([0xcc; 32], vec![0, 1]),
/// ], false).unwrap();
/// let proof_count = create_proof_of_vote_count(&tree, &Validation::Strict).unwrap();
/// let proof_node = create_inclusion_exclusion_proof(&tree, &[0xaa; 32], &Validation::Strict).unwrap();
///
/// assert_eq!(proof_count, from_json(&to_json(&proof_count).unwrap()).unwrap());
/// assert_eq!(proof_node, from_json(&to_json(&proof_node).unwrap()).unwrap());
/// ```
#[cfg(feature = "with-serde")]
pub fn to_json<T: Serialize>(data: &T) -> Result<String, String> {
    match serde_json::to_string(&data) {
        Result::Ok(v) => Ok(v),
        Result::Err(e) => Err(e.to_string()),
    }
}

/// Deserialize a node (including all child nodes) from JSON.
///
/// Example with merkle tree:
/// ```
/// use tallytree::serialize::{to_json, from_json};
/// use tallytree::generate::generate_tree;
/// use tallytree::node::NodeRef;
/// let tree = generate_tree(vec![
///     ([0xaa; 32], vec![1, 0]),
///     ([0xbb; 32], vec![0, 1]),
/// ], false).unwrap();
/// let tree_json: String = to_json(&tree).unwrap();
/// let tree_deserialized: NodeRef = from_json(&tree_json).unwrap();
/// assert_eq!(tree, tree_deserialized);
/// ```
///
/// Example with proofs:
/// ```
/// use tallytree::serialize::{to_json, from_json};
/// use tallytree::proof::*;
/// use tallytree::generate::generate_tree;
/// use tallytree::Validation;
/// let tree = generate_tree(vec![
///     ([0xaa; 32], vec![1, 0]),
///     ([0xbb; 32], vec![1, 0]),
///     ([0xcc; 32], vec![0, 1]),
/// ], true).unwrap();
/// let proof_count = create_proof_of_vote_count(&tree, &Validation::Strict).unwrap();
/// let proof_node = create_inclusion_exclusion_proof(&tree, &[0xaa; 32], &Validation::Strict).unwrap();
///
/// assert_eq!(proof_count, from_json(&to_json(&proof_count).unwrap()).unwrap());
/// assert_eq!(proof_node, from_json(&to_json(&proof_node).unwrap()).unwrap());
/// ```
#[cfg(feature = "with-serde")]
pub fn from_json<'a, T: Deserialize<'a>>(json: &'a str) -> Result<T, String> {
    match serde_json::from_str(json) {
        Result::Ok(v) => Ok(v),
        Result::Err(e) => Err(e.to_string()),
    }
}

#[cfg(test)]
#[cfg(feature = "with-serde")]
mod tests {
    use super::*;
    use crate::proof::*;
    use crate::Validation;

    #[test]
    fn test_serialize_node_json() {
        let tree = crate::generate::generate_tree(
            vec![
                ([0xaa; 32], vec![1, 0]),
                ([0xbb; 32], vec![1, 0]),
                ([0xcc; 32], vec![0, 1]),
            ],
            false,
        )
        .unwrap();
        let json = to_json(&tree).unwrap();
        let tree_deserialized = from_json(&json).unwrap();
        assert_eq!(tree, tree_deserialized);
    }

    #[test]
    fn test_serialize_count_proof() {
        let tree = crate::generate::generate_tree(
            vec![
                ([0xaa; 32], vec![1, 0]),
                ([0xbb; 32], vec![1, 0]),
                ([0xcc; 32], vec![0, 1]),
            ],
            true,
        )
        .unwrap();
        let proof = create_proof_of_vote_count(&tree, &Validation::Strict).unwrap();
        let proof_json = to_json(&proof).unwrap();
        let proof_deserialized: Proof = from_json(&proof_json).unwrap();
        assert_eq!(proof, proof_deserialized);
    }

    #[test]
    fn serialize_inclusion_exclusion_proof() {
        let tree = crate::generate::generate_tree(
            vec![
                ([0xaa; 32], vec![1, 0]),
                ([0xbb; 32], vec![1, 0]),
                ([0xcc; 32], vec![0, 1]),
            ],
            true,
        )
        .unwrap();
        let proof =
            create_inclusion_exclusion_proof(&tree, &[0xaa; 32], &Validation::Strict).unwrap();
        let proof_json = to_json(&proof).unwrap();
        let proof_deserialized: (InclusionProof, ExclusionProof) = from_json(&proof_json).unwrap();
        assert_eq!(proof, proof_deserialized);
    }
}
