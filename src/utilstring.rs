use std::fmt::Write;

/// Get a hex string representation of a u8 array.
///
/// Example:
/// ```
/// use tallytree::utilstring::to_hex;
/// assert_eq!("f00d", to_hex(&[0xf0, 0x0d]).unwrap());
/// ```
pub fn to_hex(data: &[u8]) -> Result<String, String> {
    let mut s = String::with_capacity(data.len() * 2);
    for x in data.iter() {
        if let Err(e) = write!(&mut s, "{:02x}", *x) {
            return Err(e.to_string());
        }
    }
    Ok(s)
}
